package com.growin.shuttle

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging}
import scala.concurrent.duration.{Duration, FiniteDuration}
import java.time.{Duration => JavaDuration}

object Driver {
  val shiftDuration: JavaDuration = JavaDuration.ofHours(8)
  val shiftChangeDuration: FiniteDuration = Duration((5.5*60).toLong, TimeUnit.SECONDS)
}

class Driver extends Actor with ActorLogging {
  override def receive: Receive = {
    case Message(str) => log.info(s"Received message: $str")
  }
}
