package com.growin.shuttle

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import com.growin.shuttle.Train.{Move, StationData}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{Duration, FiniteDuration}

object Main extends App {

  val actorSystem: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = actorSystem.dispatcher


  val stationData: Seq[(String, String, FiniteDuration)] = Seq(
    ("A", "B", Duration(9, TimeUnit.MINUTES)),
    ("B", "A", Duration(11, TimeUnit.MINUTES))
  )

  val shuttle = actorSystem.actorOf(Props(classOf[Train], StationData(stationData)), name = "Shuttle")
  val messenger = actorSystem.actorOf(Props(classOf[MessageSender], shuttle), name = "Messenger")

  actorSystem.scheduler.scheduleOnce(Duration(24, TimeUnit.HOURS))(actorSystem.terminate())

  shuttle ! Move
}
