package com.growin.shuttle

import java.time.{Instant, Duration => JavaDuration}
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Timers}
import akka.pattern.ask
import akka.util.Timeout
import com.growin.shuttle.Train._

import scala.collection.mutable
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.Success


object Train {

  val stationWait = Duration((7.5*60).toLong, TimeUnit.SECONDS)

  val inTransitStr = "Driver In Transit. Please try again later"

  //Train messages
  case object Move
  case object Wait
  case object WaitForDriver
  case class Station(name: String)
  case class StationData(data: Seq[(String, String, FiniteDuration)])

  //Train states
  sealed trait TrainState {
    val isInTransit: Boolean
  }
  case class AtStation(name: String) extends TrainState { override val isInTransit: Boolean = false }
  case class InTransit(from: String, to: String) extends TrainState  { override val isInTransit: Boolean = true }
}

class Train(stationData: StationData) extends Actor with Timers with ActorLogging {

  //station names and travel durations.
  var data: Seq[(String, String, FiniteDuration)] = stationData.data

  implicit val timeOut: Timeout = Timeout(Duration(10, TimeUnit.SECONDS))

  //iterator that loops through the stations in data.
  val stationsIter: Iterator[(String, String, FiniteDuration)] = Iterator.continually(data).flatten

  //shuttle location
  var state: TrainState = _

  //the starting instant of the driver's shift
  var shiftStartInstant: Instant = Instant.now

  //the driver
  val driver: ActorRef = context.actorOf(Props[Driver], name = "Driver")

  //non-urgent message queue
  var messageQueue: mutable.Queue[Message] = mutable.Queue.empty

  override def receive: Receive = {
    //changes the shuttle state to in transit.
    case Move =>
      val (from, to, dur) = stationsIter.next
      log.info(s"In transit from $from to $to.")
      state = InTransit(from, to)
      timers.startSingleTimer("movingKey", Station(to), dur)

    //changes the shuttle state to at a station.
    case Station(name) =>
      state = AtStation(name)
      log.info(s"Arrived at $name station.")
      if (messageQueue.nonEmpty) log.info("Delivering messages.")
      messageQueue.dequeueAll(_ => true).foreach(driver ! _)
      val wait = Train.stationWait +
        (if (JavaDuration.between(shiftStartInstant, Instant.now).compareTo(Driver.shiftDuration) > 0) {
          log.info("Changing shifts.")
          shiftStartInstant = Instant.now.plus(JavaDuration.ofSeconds((Train.stationWait + Driver.shiftChangeDuration).toSeconds))
          Driver.shiftChangeDuration
        } else {
          Duration.Zero
        })
      timers.startSingleTimer("stationTimerKey", Move, wait)

    //redirects a message to the driver if at a station or if urgent
    case message: Message =>
      if (state.isInTransit) {
        //ask if message is urgent.
        (sender() ? inTransitStr).mapTo[String].onComplete{
          case Success("Urgent") =>
            driver ! message.copy(str = s"${message.str} (URGENT)")
          case _ =>
            log.info(inTransitStr)
            messageQueue += message
        }(context.dispatcher)
      } else {
        driver ! message
      }

    //updates a station data. Data is only updated after a full loop of data is completed.
    case stationData: StationData => data = stationData.data
  }
}