package com.growin.shuttle

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef, Timers}
import com.growin.shuttle.MessageSender.{ResetCounter, Tick}

import scala.concurrent.duration.Duration
import scala.util.Random

object MessageSender {
  case object Tick
  case object ResetCounter
}


class MessageSender(receiver: ActorRef) extends Actor with Timers with ActorLogging {

  var urgentMessagesSent = 0

  val sendsPerHour = 5

  def paidFee: Boolean = urgentMessagesSent < 2 && Random.nextInt(2) == 1

  timers.startPeriodicTimer("counterResetTimer", ResetCounter, Duration(1, TimeUnit.HOURS))
  timers.startPeriodicTimer("ticker", Tick, Duration(60/sendsPerHour, TimeUnit.MINUTES))

  override def receive: Receive = {
    case Tick => receiver ! Message(Random.nextString(10))
    case Train.inTransitStr => if (paidFee) {
      urgentMessagesSent += 1
      sender() ! "Urgent"
    } else {
      sender() ! ""
    }
    case ResetCounter => urgentMessagesSent = 0
  }

}
